//
//  NVARDetectViewController.m
//  DPInsightDemo
//
//  Created by tangshi on 2018/11/19.
//  Copyright © 2018 MTDP. All rights reserved.
//

#import "NVARDetectViewController.h"
#import <InsightSDK/InsightSDK.h>
#import "NVARProtocol.h"


@interface NVARDetectViewController ()<InsightARDelegate,UIWebViewDelegate>
@property (nonatomic, strong)id<NVARProtocol> delegate;
@property (nonatomic, assign)CGFloat minAlertSize;
@property (nonatomic, assign)BOOL shouldReload;
@property (nonatomic, strong  )NSString *pid;
@property (nonatomic, assign)BOOL loadingAr;
@end

@implementation NVARDetectViewController
@synthesize pid = _pid;

- (instancetype)initWithPid:(NSString *)pid {

    if (self = [super init]) {
        if([pid isEqualToString:@"cloud"]) { //云识别
            Class class = NSClassFromString(@"CloudARDelegate");
            id instance = [[class alloc] init];
            if([instance conformsToProtocol:@protocol(NVARProtocol)]){
                self.delegate = instance;
                self.delegate.targetViewController = self;
            }
        } else { // pid识别
            Class class = NSClassFromString(@"NVBaseARDelegate");
            id instance = [[class alloc] init];
            if([instance conformsToProtocol:@protocol(NVARProtocol)]){
                self.delegate = instance;
                self.delegate.targetViewController = self;
                self.pid = pid;
            }
        }
    }
    return self;
}

#pragma -- mark life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    if([self.delegate respondsToSelector:@selector(targetViewDidLoad)]){
        [self.delegate targetViewDidLoad];
    }
    self.minAlertSize = 5;
    self.shouldReload = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([self.delegate respondsToSelector:@selector(targetViewWillAppear:)]){
        [self.delegate targetViewWillAppear:animated];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if([self.delegate respondsToSelector:@selector(targetViewDidAppear:)]){
        [self.delegate targetViewDidAppear:animated];
    }

    [self startARConfig];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[InsightARManager shareManager] stopAllPrepare];
    [[InsightARManager shareManager] stopInsightAR];
}
#pragma --mark
- (void)showToast:(NSString*)msg andDismiss:(BOOL)dismiss{
    if([self.delegate respondsToSelector:@selector(showToast:andDismiss:)]){
        [self.delegate showToast:msg andDismiss:dismiss];
    }
}

#pragma AR config

- (void)startARConfig{
    if(self.loadingAr) return;
    if ([InsightARManager isSDKSupported]) {
        self.loadingAr = YES;
        if([self isTestBundle]){
            [[InsightARManager shareManager]registerAppKey:@"AR-B0AA-3EE17A6DRD6B-i-t" appSecret:@"eDapRuEG8I"]; //DemoForSDK测试环境
        }else{
            [[InsightARManager shareManager]registerAppKey:@"AR-A4B6-CE9493B9AB7A9-i-f" appSecret:@"RB7qBcf09v"]; //SDK正式环境
        }
        if([self.delegate arDetectType] == ARDetectTypeClound){
            [self cloudARStart];
        }else{
            [self startPidInit];
        }
    }else{
        [self showToast:@"您的系统暂不支持AR" andDismiss:YES];
    }
}

- (void)cloudARStart
{
    ARAlgPrepareOption *algOptions = [ARAlgPrepareOption new];
    algOptions.updateTimeInterval = 300;
    algOptions.getLastestVersion = YES;
    [[InsightARManager shareManager] prepareAlgorithmModel:algOptions downloadProgress:^(NSProgress *downloadProgress) {
    } completion:^(NSError *error) {
        if (error) {
            self.loadingAr = NO;
            [self showToast:@"初始化资源失败，请稍后再试" andDismiss:YES];
        }else{
            ARAlgorithmModel *arcloudProduct =  [[InsightARManager shareManager] getAlgorithmModel];
            [[InsightARManager shareManager] startInsightARWithCloud:arcloudProduct withARDelegate:self];
        }
    }];
}

- (void)startPidInit{
    ARProduct *product = [[InsightARManager shareManager] getARProduct:self.pid];
    if (product.downloaded) {
        [[InsightARManager shareManager] checkProductStatus:^(NSError *error) {
            self.loadingAr = NO;
            [self showToast:@"检查资源失败，请稍后再试" andDismiss:YES];
        } result:^(NSArray<ARProduct *> *products) {
            for(int i = 0 ;i < products.count;i ++){//检查大小，需要打点记录
                ARProduct *product = products[i];
                if([product.pid isEqualToString:self.pid]){
                    [self startAR:(int)product.size];
                    return ;
                }
            }
            [self startAR:0];
        }];
    }else{
        [self checkProductSize];
    }
}


- (void)checkProductSize{
    NSMutableSet *set = [NSMutableSet new];
    [set addObject:self.pid];
    [[InsightARManager shareManager] getProductSize:set productList:^(NSError *error) {
        self.loadingAr = NO;
        [self showToast:@"资源获取失败，请稍后再试" andDismiss:YES];
        return ;
    } result:^(NSDictionary *dictionary) {
        __block int size = [dictionary[self.pid] intValue];

        [[InsightARManager shareManager] checkAlgorithmModel:^(NSError *error) {
            self.loadingAr = NO;
            [self showToast:@"资源获取失败，请稍后再试" andDismiss:YES];
            return ;
        } result:^(ARAlgorithmModel *algorithmModel) {
            size += algorithmModel.size;
            if(size/(1024*1024) > self.minAlertSize){
                NSString * msg = nil;
                msg = [NSString stringWithFormat:@"当前处于非wifi状态，加载内容需要消耗\n %.2fM流量",((CGFloat)size)/((CGFloat)1024*1024)];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"下载提醒" message:msg preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消下载" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    [self showToast:@"停止加载，即将返回。。。" andDismiss:YES];
                }];
                UIAlertAction *loginAction = [UIAlertAction actionWithTitle:@"继续下载" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self startAR:size];
                }];
                [alert addAction:cancel];
                [alert addAction:loginAction];
                [self presentViewController:alert animated:YES completion:^{

                }];
            }else{
                [self startAR:size];
            }
        }];
    }];
}

- (void)startAR:(int)size{
    CFAbsoluteTime startTime = CFAbsoluteTimeGetCurrent();
    ARPrepareOptions *options = [ARPrepareOptions new];
    options.dynamicDownloadMode = [self.delegate dynamicDowload];
    options.productID =  self.pid;
    options.protocolClasses = nil; //设置代理服务器
    [[InsightARManager shareManager] prepareARWithOptions:options downloadProgress:^(NSProgress *downloadProgress) {
        [self loading:downloadProgress];
    } completion:^(NSError *error) {
        NSLog(@"completion End Time %@" ,@(CFAbsoluteTimeGetCurrent() - startTime));
        if (error != nil) {
            self.loadingAr = NO;
            [self showToast:@"AR初始化失败" andDismiss:YES];
            return ;
        }else{
            self.loadingAr = NO;
            ARProduct *product = [[InsightARManager shareManager]getARProduct:options.productID];
            if(self.shouldReload){
                [[InsightARManager shareManager] reloadInsightAR:product];
                self.shouldReload = NO;
            }else{
                [[InsightARManager shareManager] startInsightAR:product withARDelegate:self];
            }
        }
    }];
}


#pragma --mark  ar delegate

- (void)insightARDidUpdateARState:(InsightARState)state{
    switch (state) {
        case InsightARStateInitOK:{
            self.delegate.arView = [[InsightARManager shareManager] getInsightARView];
            [self.view insertSubview:self.delegate.arView atIndex:0];
        }
            break;
        case InsightARStateInitFail:{

        }
            break;

        case InsightARStateDetecting:{

        }
            break;

        case InsightARStateTrackLimited:{

        }
            break;

        case InsightARStateTrackingNormal:{

        }
            break;

        case InsightARStateTrackLost:{

        }
            break;

        default:
            break;
    }
    if([self.delegate respondsToSelector:@selector(insightARDidUpdateARState:)]){
        [self.delegate insightARDidUpdateARState:state];
    }
}

- (void)insightAREvent:(InsightAREvent *)event{
    switch (event.type) {
        case InsightAREvent_Function_ReloadARProduct:{
            if(self.shouldReload) return;//上一个场景正在加载
            NSString *productID  = [event.params objectForKey:@"name"];
            if([productID isKindOfClass:[NSString class]] && productID.length >0){
                self.pid = productID;
            }
            self.shouldReload = YES;
            [self startPidInit];
            break;
        }
        default:
            break;
    }
    if([self.delegate respondsToSelector:@selector(insightAREvent:)]){
        [self.delegate insightAREvent:event];
    }
}

#pragma --mark

- (NSString *)pid{
    if([self.delegate respondsToSelector:@selector(pid)]){
        return [self.delegate pid];
    }
    return @"";
}

- (void)setPid:(NSString *)pid{
    if([self.delegate respondsToSelector:@selector(setPid:)]){
        [self.delegate setPid:pid];
    }
}

- (void)loading:(NSProgress*)progress{
    if([self.delegate respondsToSelector:@selector(loading:)]){
        [self.delegate loading:progress];
    }
}

- (BOOL)isTestBundle{
    return [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"] isEqualToString:@"com.dianping.ba.dpscope"];
}

@end

