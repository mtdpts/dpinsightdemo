//
//  ViewController.m
//  DPInsightDemo
//
//  Created by tangshi on 2018/11/19.
//  Copyright © 2018 MTDP. All rights reserved.
//

#import "ViewController.h"
#import "NVAVCaptureScanViewController.h"

@interface ViewController ()<CaptureScanViewDelegate>
@property (nonatomic, strong) NVAVCaptureScanViewController *avCaptureScanViewController;
@end

@implementation ViewController

- (void)dealloc {
    _avCaptureScanViewController.delegate = nil;
    _avCaptureScanViewController = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"扫一扫";
    [self drawerAnimationDidEnd:self.navigationController];
}

- (void)drawerAnimationDidEnd:(UINavigationController *)navigationController
{
    if (!self.avCaptureScanViewController) {
        self.avCaptureScanViewController = [[NVAVCaptureScanViewController alloc] init];
        self.avCaptureScanViewController.delegate = self;
        [self addChildViewController:self.avCaptureScanViewController];
        [self.view addSubview:self.avCaptureScanViewController.view];
        [self.avCaptureScanViewController didMoveToParentViewController:self];
    }
}

#pragma mark - CaptureScanViewDelegate
- (void)captureScanViewController:(UIViewController *)controller didScanResult:(NSString *)result {
    NSLog(@"NVQRCodeScan, redirected Done.");
}

@end
