//
//  NVAVCaptureScanViewController.m
//  DPInsightDemo
//
//  Created by tangshi on 2018/11/19.
//  Copyright © 2018 MTDP. All rights reserved.
//

#import "NVAVCaptureScanViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMotion/CoreMotion.h>
#import <ImageIO/ImageIO.h>
#import "UIView+Layout.h"
#import "R_QRCode.h"
#import "NVARDetectViewController.h"

#define MAX_ZOOM 3.5f

@interface NVAVCaptureScanViewController()<AVCaptureMetadataOutputObjectsDelegate, AVCaptureVideoDataOutputSampleBufferDelegate> {
    uint64_t t_frame, t_fps, t_scan;
    CGFloat dt_frame;
}

@property (nonatomic, strong) AVCaptureDevice * device;
@property (nonatomic, strong) AVCaptureDeviceInput * input;
@property (nonatomic, strong) AVCaptureMetadataOutput * metaDataOutput;
@property (nonatomic, strong) AVCaptureSession * session;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer * preview;
@property (nonatomic, strong) NSString * displayedMessage;
@property (nonatomic, strong) UIImageView * scanLine, *cropRectCornerLTImageView, *cropRectCornerLBImageView, *cropRectCornerRTImageView, *cropRectCornerRBImageView;
@property (nonatomic, assign) BOOL showLicenseButton, isDeviceStable;
@property (nonatomic, assign) CGRect cropRect;

@property (nonatomic, strong) CMMotionManager *motionManager;
@property (nonatomic) CMSampleBufferRef failedImageBuffer;
@property (nonatomic, assign) CGFloat zoomFactor, frameCount, prevZoomFactor, deviceMaxZoomFactor;
@property (nonatomic, strong) dispatch_queue_t cameraQueue;
@property (nonatomic, strong) UIButton *flashButton;
@property (nonatomic, strong) UIView *flashBottomPanel, *maskView;
@property (nonatomic, strong) UILabel *hintLabel, *flashSwitchLabel;
@property (nonatomic, strong) CAShapeLayer *cropAreaLayer, *maskLayer;
@property (nonatomic, strong) UIButton *arScanButton;
@property (nonatomic, strong) UILabel *arScanLabel;


@end

@implementation NVAVCaptureScanViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    _delegate = nil;
}


- (id)init {
    self = [super init];
    _cameraQueue = dispatch_queue_create("com.dianping.cameraOPQ", NULL);
    return self;
}

- (void)didEnterIntoBackGround:(NSNotification *)nofification {
    [self stopCamera];
}

- (void)didBecomeActiveNotification:(NSNotification *)notification {
    [self startCamera];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:69.0f / 255 green:69.0f / 255 blue:69.0f / 255 alpha:1];
    self.title = @"扫一扫";
    [self setUpInitials];
}

- (BOOL)checkPermission {
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        return YES;
    } else if(authStatus == AVAuthorizationStatusDenied){
        return NO;
    } else if(authStatus == AVAuthorizationStatusRestricted){
        return NO;
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        return YES;
    }
    return NO;
}

- (void)setUpInitials {
    self.cropRect = CGRectZero;
    [self initSubViews];
    [self setupCamera];
    [self setupMotionManager];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterIntoBackGround:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didBecomeActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [self startCamera];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self saveOrUploadFailedImages];
    [self stopCamera];
}

-  (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (NSString *)getQRImageSavePath{
    NSString *savePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"QRImages"];
    NSFileManager *fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:savePath]) {
        [fm createDirectoryAtPath:savePath withIntermediateDirectories:YES attributes:nil error:NULL];
        [self addSkipBackupAttributeToItemAtPath:savePath];
    }
    return savePath;
}

- (BOOL)addSkipBackupAttributeToItemAtPath:(NSString *)filePathString {
    NSURL* URL= [NSURL fileURLWithPath: filePathString];
    assert([[NSFileManager defaultManager] fileExistsAtPath:[URL path]]);

    NSError *error = nil;
    BOOL success = [URL setResourceValue:[NSNumber numberWithBool:YES]
                                  forKey:NSURLIsExcludedFromBackupKey
                                   error:&error];
    if(!success) {
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

- (void)removeOldestFileFromDir:(NSString *)dir forFileManager:(NSFileManager *)fm {
    NSError *error = nil;
    NSArray *contents = [fm contentsOfDirectoryAtPath:dir error:&error];
    NSArray *jpgFiles = [contents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.jpg'"]];

    NSDate *oldest = [NSDate date];
    NSString *oldestFileName = nil;

    if (jpgFiles.count <= 20) {
        return;
    }

    for (NSString *f in jpgFiles) {
        NSString *photoPath = [dir stringByAppendingPathComponent:f];

        NSDate *created = [[fm attributesOfItemAtPath:photoPath error:&error] objectForKey:@"NSFileCreationDate"];

        if([created compare:oldest] == NSOrderedAscending){
            oldestFileName = [NSString stringWithString:photoPath];
            oldest = created;
        }
    }

    [fm removeItemAtPath:oldestFileName error:&error];
}

- (void)saveOrUploadFailedImages {

    if (!self.failedImageBuffer) {
        return;
    }

    CGImageRef failedImageRef = [self imageFromSampleBuffer:self.failedImageBuffer];

    if (!failedImageRef) {
        return;
    }

    CGRect validCropRect = [self.preview metadataOutputRectOfInterestForRect:self.cropRect];
    CGFloat imgWidth = CGImageGetWidth(failedImageRef);
    CGFloat imgHeight = CGImageGetHeight(failedImageRef);
    CGImageRef croppedImage = CGImageCreateWithImageInRect(failedImageRef, [self increaseRect:CGRectMake(validCropRect.origin.x * imgWidth, validCropRect.origin.y * imgHeight, validCropRect.size.width * imgWidth, validCropRect.size.height * imgHeight) byPercentage:0.15]);

    [self removeOldestFileFromDir:[self getQRImageSavePath] forFileManager:[NSFileManager defaultManager]];

    UIImage *imageToSave = [self rotateImage:croppedImage clockwise:YES];
    NSString *imageName = [[self getQRImageSavePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"qrimage.jpg"]];
    NSData *binaryImageData = UIImageJPEGRepresentation(imageToSave, 0);

    [binaryImageData writeToFile:imageName atomically:YES];

    CGImageRelease(croppedImage);
    CGImageRelease(failedImageRef);
}

- (void)initSubViews {
    self.cropAreaLayer = [CAShapeLayer layer];
    self.cropAreaLayer.strokeColor = [UIColor whiteColor].CGColor;
    self.cropAreaLayer.lineWidth = 2;
    self.cropAreaLayer.fillColor = [UIColor clearColor].CGColor;
    [self.view.layer addSublayer:self.cropAreaLayer];

    self.maskView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.maskView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.5]];
    [self.view addSubview:self.maskView];

    self.maskLayer = [CAShapeLayer layer];
    [self.maskView.layer setMask:self.maskLayer];

    self.displayedMessage = @"将二维码/条码放入框内，即可扫码";

    UIFont * font = [UIFont systemFontOfSize:14];
    UILabel * noteLabel = [[UILabel alloc] init];
    noteLabel.backgroundColor = [UIColor clearColor];
    noteLabel.textColor = [UIColor whiteColor];
    noteLabel.font = font;
    noteLabel.numberOfLines = 2;
    noteLabel.textAlignment = NSTextAlignmentCenter;
    noteLabel.text = self.displayedMessage;
    self.hintLabel = noteLabel;
    [self.view addSubview:noteLabel];

    self.scanLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Scan_Line"]];
    [self.view addSubview:self.scanLine];

    UIImageView * arrowTL = [[UIImageView alloc] initWithImage:
                             [UIImage imageNamed:@"Arrow_Top_Left"]];

    [self.view addSubview:arrowTL];
    self.cropRectCornerLTImageView = arrowTL;

    UIImageView * arrowTR = [[UIImageView alloc] initWithImage:
                             [UIImage imageNamed:@"Arrow_Top_Right"]];
    [self.view addSubview:arrowTR];
    self.cropRectCornerRTImageView = arrowTR;

    UIImageView * arrowBL = [[UIImageView alloc] initWithImage:
                             [UIImage imageNamed:@"Arrow_Bottom_Left"]];
    [self.view addSubview:arrowBL];
    self.cropRectCornerLBImageView = arrowBL;

    UIImageView * arrowBR = [[UIImageView alloc] initWithImage:
                             [UIImage imageNamed:@"Arrow_Bottom_Right"]];
    [self.view addSubview:arrowBR];
    self.cropRectCornerRBImageView = arrowBR;

    self.preview = [AVCaptureVideoPreviewLayer layer];
    self.preview.videoGravity = AVLayerVideoGravityResizeAspectFill;

    self.flashBottomPanel = [[UIView alloc] initWithFrame:CGRectZero];
    self.flashBottomPanel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    [self.view addSubview:self.flashBottomPanel];

    self.flashButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.flashButton setImage:[UIImage imageNamed:R_flash_light_on] forState:UIControlStateNormal];
    [self.flashButton setImage:[UIImage imageNamed:R_flash_light_off] forState:UIControlStateSelected];
    [self.flashButton addTarget:self action:@selector(triggerFlash) forControlEvents:UIControlEventTouchUpInside];

    self.flashSwitchLabel = [UILabel new];
    self.flashSwitchLabel.text = @"打开闪光灯";
    self.flashSwitchLabel.textColor = [UIColor whiteColor];
    self.flashSwitchLabel.textAlignment = NSTextAlignmentCenter;
    self.flashSwitchLabel.font = [UIFont systemFontOfSize:13];

    [self.flashBottomPanel addSubview:self.flashButton];
    [self.flashBottomPanel addSubview:self.flashSwitchLabel];

    self.arScanButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.arScanButton setImage:[UIImage imageNamed:R_arscan_logo] forState:UIControlStateNormal];
    self.arScanButton.frame = CGRectMake(0, 0, 54, 54);
    [self.flashBottomPanel addSubview:self.arScanButton];

    [self.arScanButton addTarget:self action:@selector(gotoARScan:) forControlEvents:UIControlEventTouchUpInside];
    self.arScanLabel = [UILabel new];
    self.arScanLabel.textColor = [UIColor whiteColor];
    self.arScanLabel.text = @"AR扫描";
    self.arScanLabel.font = [UIFont systemFontOfSize:13];
    self.arScanLabel.textAlignment = NSTextAlignmentCenter;
    [self.flashBottomPanel addSubview:self.arScanLabel];


    [self.view.layer insertSublayer:self.preview atIndex:0];

    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapToZoomInOut:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTapGesture];
    [self.view addGestureRecognizer:[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchToZoom:)]];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    if (CGRectEqualToRect(self.cropRect, CGRectZero)) {
        float kPadding = 50;
        CGFloat rectSize = self.view.frame.size.width - kPadding * 2;
        self.cropRect = CGRectMake(kPadding, (self.view.frame.size.height - rectSize) / 2 - 20, rectSize, rectSize);

        UIBezierPath *squarePath = [UIBezierPath bezierPathWithRect:self.cropRect];
        self.cropAreaLayer.path = squarePath.CGPath;

        self.maskView.frame = self.view.bounds;
        UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.view.bounds];
        [path appendPath:[[UIBezierPath bezierPathWithRoundedRect:self.cropRect cornerRadius:0] bezierPathByReversingPath]];
        self.maskLayer.path = path.CGPath;

        CGSize constraint = CGSizeMake(self.cropRect.size.width, 100);
        CGSize displaySize = [self.displayedMessage boundingRectWithSize:constraint options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.hintLabel.font} context:nil].size;
        CGRect displayRect = CGRectMake((self.view.frame.size.width - displaySize.width) / 2 - 10 , CGRectGetMaxY(self.cropRect)  + 10, displaySize.width + 20, displaySize.height + 10);

        CGRect displayRectLb = CGRectMake(displayRect.origin.x + 5, displayRect.origin.y + 2, displayRect.size.width - 10, displayRect.size.height - 4);

        self.hintLabel.frame = displayRectLb;

        CGRect rct;
        rct.origin.x = self.cropRect.origin.x + 4;
        rct.origin.y = self.cropRect.origin.y;
        rct.size.width = self.cropRect.size.width - 8;
        rct.size.height = 5;
        self.scanLine.frame = rct;

        float padding = 1;
        rct = self.cropRectCornerLTImageView.frame;
        rct.origin.x = self.cropRect.origin.x - padding;
        rct.origin.y = self.cropRect.origin.y - padding;
        self.cropRectCornerLTImageView.frame = rct;

        rct = self.cropRectCornerRTImageView.frame;
        rct.origin.x = CGRectGetMaxX(self.cropRect) - rct.size.width + padding;
        rct.origin.y = self.cropRect.origin.y - padding;
        self.cropRectCornerRTImageView.frame = rct;

        rct = self.cropRectCornerLBImageView.frame;
        rct.origin.x = self.cropRect.origin.x - padding;
        rct.origin.y = CGRectGetMaxY(self.cropRect) - rct.size.height + padding;
        self.cropRectCornerLBImageView.frame = rct;

        rct = self.cropRectCornerRBImageView.frame;
        rct.origin.x = CGRectGetMaxX(self.cropRect) - rct.size.width + padding;
        rct.origin.y = CGRectGetMaxY(self.cropRect) - rct.size.height + padding;
        self.cropRectCornerRBImageView.frame = rct;

        self.preview.frame = self.view.bounds;

        self.flashBottomPanel.frame = CGRectMake(0, self.view.height - 129.0, self.view.width, 129.0);

        self.flashButton.frame = CGRectMake(0, 0, 70, 70);
        self.flashButton.center = CGPointMake(self.flashBottomPanel.centerX - 25 - self.flashButton.frame.size.width/2, self.flashButton.height / 2.0f + 3);

        self.flashSwitchLabel.frame = CGRectMake(0, 0, 70, 25);
        self.flashSwitchLabel.center = CGPointMake(self.flashButton.center.x, self.flashButton.height + self.flashSwitchLabel.height / 2.0f - 3);

        self.arScanButton.frame = CGRectMake(0, 0, 70, 70);
        self.arScanButton.center = CGPointMake(self.flashBottomPanel.centerX +25 + self.arScanButton.frame.size.width/2,  self.flashButton.centerY);
        self.arScanLabel.frame = CGRectMake(0, 0, 70, 25);
        self.arScanLabel.center = CGPointMake(self.arScanButton.centerX,  self.arScanButton.height + self.arScanLabel.height / 2.0f - 3);


        [self startScanAnimation];
    }

}

- (void)gotoARScan:(id)sender {
    [self stopCamera];
    NVARDetectViewController *vc = [[NVARDetectViewController alloc] initWithPid:@"cloud"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)triggerFlash {
    AVCaptureTorchMode mode = self.device.torchMode;
    AVCaptureTorchMode nextMode = AVCaptureTorchModeOff;

    switch (mode) {
        case AVCaptureTorchModeOn:
        case AVCaptureTorchModeAuto:
            nextMode = AVCaptureTorchModeOff;
            break;

        case AVCaptureTorchModeOff:
            nextMode = AVCaptureTorchModeOn;
            break;

        default:
            break;
    }
    if ([self.device lockForConfiguration:nil]) {
        self.device.torchMode = nextMode;
        [self.device unlockForConfiguration];

        self.flashButton.selected = nextMode == AVCaptureTorchModeOn;
        self.flashSwitchLabel.text = self.flashButton.selected ? @"关闭闪光灯" : @"打开闪光灯";
    }
}

- (void)startScanAnimation {
    if ([self.scanLine.layer animationForKey:@"scanLine"]) {
        [self.scanLine.layer removeAnimationForKey:@"scanLine"];
    }

    CALayer * scanLayer = self.scanLine.layer;
    CABasicAnimation *positionAnimation  = [CABasicAnimation animationWithKeyPath:@"position"];
    positionAnimation.fromValue =  [NSValue valueWithCGPoint:scanLayer.position];
    CGPoint toPoint = scanLayer.position;
    toPoint.y += self.cropRect.size.height - self.scanLine.frame.size.height;
    positionAnimation.toValue = [NSValue valueWithCGPoint:toPoint];
    positionAnimation.duration = 2.5;
    positionAnimation.repeatCount = INT8_MAX;
    positionAnimation.autoreverses = YES;
    [scanLayer addAnimation:positionAnimation forKey:@"scanLine"];
}

- (void)stopScanAnimation {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.scanLine.layer removeAnimationForKey:@"scanLine"];
    });
}

- (void)startCamera {
    if (![self checkPermission]) {
        NSLog(@"无法使用相机");
    }

    dispatch_async(self.cameraQueue, ^{
        if (!self.session.isRunning) {
            [self.session startRunning];
        }
    });

    [self startScanAnimation];
}

- (void)stopCamera {
    [self stopScanAnimation];

    if (self.session && self.session.isRunning) {
        dispatch_async(self.cameraQueue, ^{
            [self.session stopRunning];
        });
    }

    self.flashButton.selected = NO;
    self.flashSwitchLabel.text = @"打开闪光灯";
    if (![self.device isTorchModeSupported:AVCaptureTorchModeOff]) {
        return;
    }
    if ([self.device lockForConfiguration:nil]) {
        self.device.torchMode = AVCaptureTorchModeOff;
        [self.device unlockForConfiguration];
    }
}

-(void)setupMotionManager {
    self.motionManager = [[CMMotionManager alloc] init];
    if (self.motionManager.accelerometerAvailable) {
        self.motionManager.deviceMotionUpdateInterval = 1 / 60.f;
        [self.motionManager startDeviceMotionUpdatesToQueue:[[NSOperationQueue alloc] init] withHandler:^(CMDeviceMotion * _Nullable motion, NSError * _Nullable error) {
            if (!error && (fabs(motion.userAcceleration.x) + fabs(motion.userAcceleration.y) + fabs(motion.userAcceleration.z)) / 3.0f <= 0.025) {
                self.isDeviceStable = YES;
            } else {
                self.isDeviceStable = NO;
            }
        }];
    }
}


- (void)setupCamera {
#if HAS_AVFF
    if (self.session) {
        return;
    }

    dispatch_async(self.cameraQueue, ^{
        self.device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        self.input = [AVCaptureDeviceInput deviceInputWithDevice:self.device error:nil];

        self.session = [[AVCaptureSession alloc] init];
        if ([self.session canSetSessionPreset:AVCaptureSessionPreset1280x720]) {
            [self.session setSessionPreset:AVCaptureSessionPreset1280x720];
        }
        if ([self.session canAddInput:self.input])
        {
            [self.session addInput:self.input];
        }

        [self addDataOutputToSession];

        self.deviceMaxZoomFactor = MIN(self.device.activeFormat.videoMaxZoomFactor, MAX_ZOOM);

        self.zoomFactor = self.device.videoZoomFactor;

        if (self.preview) {
            self.preview.session = self.session;
            if (self.metaDataOutput) {
                CGRect validCropRect = [self.preview metadataOutputRectOfInterestForRect:self.cropRect];
                self.metaDataOutput.rectOfInterest = validCropRect;
            }
        }

        if (!self.device.hasTorch || !self.device.hasFlash || !self.device.torchAvailable) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.flashBottomPanel removeFromSuperview];
            });
        }
    });
#endif
}

-(void)addDataOutputToSession {
    // add videooutput for getting buffer image
    AVCaptureVideoDataOutput *output = [[AVCaptureVideoDataOutput alloc] init];
    output.alwaysDiscardsLateVideoFrames = YES;
    [self.session addOutput:output];

    // Configure your output.
    dispatch_queue_t queue = dispatch_queue_create("qrcodebuffer", NULL);
    [output setSampleBufferDelegate:self queue:queue];
}

- (void)doubleTapToZoomInOut:(UITapGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded) {
        CGPoint tapLocation = [gesture locationInView:self.view];
        tapLocation = [self.view convertPoint:tapLocation toView:self.flashBottomPanel];
        if ([self.flashBottomPanel pointInside:tapLocation withEvent:nil]) {
            return;
        }
        self.zoomFactor = self.device.videoZoomFactor > 1 ? 1 : self.deviceMaxZoomFactor;
        NSError *error;
        if ([self.device lockForConfiguration:&error]) {
            [self.device rampToVideoZoomFactor:self.zoomFactor withRate:4.0];
            [self.device unlockForConfiguration];
        }
    }
}

- (void)pinchToZoom:(UIPinchGestureRecognizer *)gesture {
    AVCaptureDevice *device = self.device;
    switch (gesture.state) {
        case UIGestureRecognizerStateBegan:
            self.prevZoomFactor = device.videoZoomFactor;
            break;

        case UIGestureRecognizerStateChanged: {
            NSError *error;
            [device lockForConfiguration:&error];
            if (!error) {
                device.videoZoomFactor = MAX(1, MIN(gesture.scale * self.prevZoomFactor, self.deviceMaxZoomFactor));
                [device unlockForConfiguration];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    NSString * result = nil;
    if ([metadataObjects count] > 0)
    {
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex:0];
        result = [metadataObject.stringValue copy];
    }
    // dispatch to main queue
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopCamera];
        [self.delegate captureScanViewController:self didScanResult:result];
    });
}

- (CGRect)increaseRect:(CGRect)rect byPercentage:(CGFloat) percent {
    return CGRectInset(rect, -CGRectGetWidth(rect) * percent / 2, -CGRectGetHeight(rect) * percent / 2);
}

-(void)setFailedImageBuffer:(CMSampleBufferRef)failedImageBuffer {
    if (_failedImageBuffer) {
        CFRelease(_failedImageBuffer);
    }
    if (!failedImageBuffer) {
        _failedImageBuffer = nil;
        return;
    }
    OSStatus status = CMSampleBufferCreateCopy(kCFAllocatorDefault, failedImageBuffer, &_failedImageBuffer);
    if (noErr != status) {
        _failedImageBuffer = nil;
    }
}

- (BOOL)isInDarkEnv:(CMSampleBufferRef)inputImage {
    BOOL isDark = FALSE;

    CVPixelBufferRef pixelBuffer =
    CMSampleBufferGetImageBuffer(inputImage);

    CVPixelBufferLockBaseAddress( pixelBuffer, 0 );

    size_t bufferWidth = CVPixelBufferGetWidth(pixelBuffer);
    size_t bufferHeight = CVPixelBufferGetHeight(pixelBuffer);
    size_t rowBytes = CVPixelBufferGetBytesPerRow(pixelBuffer);
    long pixelBytes = rowBytes/bufferWidth;

    NSInteger blue = 0, green = 0, red = 0;

    float const colorM = bufferHeight * bufferWidth;
    unsigned char *base = (unsigned char *)CVPixelBufferGetBaseAddress(pixelBuffer);

    for( int row = 0; row < bufferHeight; row++ ) {
        for( int column = 0; column < bufferWidth; column++ ) {

            unsigned char *pixel = base + (row * rowBytes) +
            (column * pixelBytes);
            // BGRA pixel format
            blue += pixel[0];
            green += pixel[1];
            red += pixel[2];
        }
    }

    CVPixelBufferUnlockBaseAddress( pixelBuffer, 0 );

    float luminance = (red * 0.299 + green * 0.587 + blue * 0.114) / colorM;
    if (luminance < 115)
        isDark = YES;

    return isDark;
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didDropSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    NSLog(@"Sample was dropped...");
}

- (CGImageRef)imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer {
    @autoreleasepool {
        CFArrayRef attachments = CMSampleBufferGetSampleAttachmentsArray(sampleBuffer, YES);
        if (attachments) {
            CFMutableDictionaryRef dict = (CFMutableDictionaryRef)CFArrayGetValueAtIndex(attachments, 0);
            if (CFDictionaryGetValue(dict, kCMSampleBufferAttachmentKey_DroppedFrameReason) != NULL) {
                return nil;
            }
        }

        CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        CIImage   *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];
        return [[CIContext contextWithOptions:@{kCIContextUseSoftwareRenderer: @NO}]
                createCGImage:ciImage
                fromRect:CGRectMake(0, 0,
                                    CVPixelBufferGetWidth(pixelBuffer),
                                    CVPixelBufferGetHeight(pixelBuffer))];
    }
}

- (UIImage*)rotateImage:(CGImageRef)sourceImage clockwise:(BOOL)clockwise
{
    CGSize size = CGSizeMake(CGImageGetWidth(sourceImage), CGImageGetHeight(sourceImage));
    UIGraphicsBeginImageContext(CGSizeMake(size.height, size.width));
    [[UIImage imageWithCGImage:sourceImage
                         scale:1.0
                   orientation:clockwise ? UIImageOrientationRight : UIImageOrientationLeft]
     drawInRect:CGRectMake(0,0,size.height ,size.width)];

    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return newImage;
}

@end
