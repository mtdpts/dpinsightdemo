//
//  R_QRCode.h
//  DPInsightDemo
//
//  Created by tangshi on 2018/11/19.
//  Copyright © 2018 MTDP. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * R_arscan_back;
extern NSString * R_arscan_button_blue;
extern NSString * R_arscan_button_orange;
extern NSString * R_arscan_close_gray;
extern NSString * R_arscan_close_white;
extern NSString * R_arscan_innerring;
extern NSString * R_arscan_logo;
extern NSString * R_arscan_mask;
extern NSString * R_arscan_outerring;
extern NSString * R_flash_light_off;
extern NSString * R_flash_light_on;
