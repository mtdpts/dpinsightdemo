//
//  NVAVCaptureScanViewController.h
//  DPInsightDemo
//
//  Created by tangshi on 2018/11/19.
//  Copyright © 2018 MTDP. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CaptureScanViewDelegate <NSObject>

@required
- (void)captureScanViewController:(UIViewController *)controller didScanResult:(NSString *)result;

@optional
- (void)captureScanViewControllerFailed:(UIViewController *)controller;

@end

typedef void(^ScanResult)(NSString *result);

#if !TARGET_IPHONE_SIMULATOR
#define HAS_AVFF 1
#endif

@interface NVAVCaptureScanViewController : UIViewController

@property (nonatomic, weak) id<CaptureScanViewDelegate> delegate;

@end


NS_ASSUME_NONNULL_END
