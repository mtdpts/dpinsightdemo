//
//  NVARDetectViewController.h
//  DPInsightDemo
//
//  Created by tangshi on 2018/11/19.
//  Copyright © 2018 MTDP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NVARDetectViewController : UIViewController
@property (nonatomic, assign) UIInterfaceOrientation orientation;

- (instancetype)initWithPid:(NSString *)pid;

@end
