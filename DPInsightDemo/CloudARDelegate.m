//
//  NVBaseARDelegate.m
//  DPInsightDemo
//
//  Created by tangshi on 2018/11/19.
//  Copyright © 2018 MTDP. All rights reserved.
//

#import "CloudARDelegate.h"
#import "NVARProtocol.h"

@interface CloudARDelegate()
@property (nonatomic, strong) UILabel *toastLab;
@property (nonatomic, assign) BOOL isDismissing;

@property (nonatomic, strong)UIButton *closeWebBtn;
@property (nonatomic, strong)UIView *bgView;
@property (nonatomic, strong)UIView *preview;
@property (nonatomic, strong)UIImageView *previewImg;
@property (nonatomic, strong)UIButton *redrawBtn;
@property (nonatomic, strong)NSDictionary *shareData;
@end
static CGFloat btnWidth = 35;

@implementation CloudARDelegate {
    UILabel *_label;
    UIButton *_closeBtn;
    BOOL _shouldPop2Home;
}

@synthesize pid = _pid;


- (instancetype)init{
    if(self = [super init]){

    }
    return self;
}


- (void)targetViewDidLoad {
    _closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(30, 50, 40, 25)];
    [_closeBtn setTitle:@"关闭" forState:UIControlStateNormal];
    [_closeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_closeBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.targetViewController.view addSubview:_closeBtn];

    _label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.targetViewController.view.frame.size.width -40, 150)];
    _label.center = self.targetViewController.view.center;
    _label.hidden = YES;
    _label.textAlignment = NSTextAlignmentCenter;
    [self.targetViewController.view addSubview:_label];
}

-(void)targetViewWillAppear:(BOOL)animated{

}

-(void)targetViewDidAppear:(BOOL)animated{
    _shouldPop2Home = YES;
    [self shouldPop2Home];
}


#pragma --mark ar event

- (void)insightAREvent:(InsightAREvent *)event{
    InsightAREventType type = event.type;
    switch (type) {
        case InsightAREvent_Function_ReloadARProduct: {
            dispatch_async(dispatch_get_main_queue(), ^{
                _closeBtn.hidden = YES;
            });
            break;
        }
        case InsightAREvent_Function_RunScript:{
            [self runScript:event];
            break;
        }
        case InsightAREvent_Function_Share:{
            self.shareData = event.params;
            [self showPreView];
            break;
        }
        case InsightAREvent_Function_OpenURL:{
            NSString *url =  event.params[@"url"];;
            NSString *jsonString =  event.params[@"type"];;
            NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
            NSError *serialError = nil;
            NSDictionary *param = [NSJSONSerialization JSONObjectWithData:data options:0 error:&serialError];
            [self loadUrl:url param:param];
            break;
        }
        case InsightAREvent_Function_ScreenShot:{

            break;
        }
        case InsightAREvent_Function_CloseARProduct:{
            [self back];
            break;
        }
        default:
            break;
    }
}

- (void)insightARDidUpdateARState:(InsightARState)state {
    switch (state) {
        case InsightARStateInitOK:{

        }
            break;
        case InsightARStateInitFail:{

        }
            break;

        case InsightARStateDetecting:{

        }
            break;

        case InsightARStateTrackLimited:{

        }
            break;

        case InsightARStateTrackingNormal:{

        }
            break;

        case InsightARStateTrackLost:{

        }
            break;

        default:
            break;
    }
}


#pragma --mark business events

- (void)runScript:(InsightAREvent *)event{
    NSString *script = event.params[@"script"];
    NSString *param = event.params[@"json"];

    [[InsightARManager shareManager] runInsightARScript:script param:param callback:^(BOOL result) {
        NSLog(@"runInsightARScript callback result: %@", @(result));
    }];
}

- (void)loadUrl:(NSString *)url param:(NSDictionary *)param{
    if(!param && ![param isKindOfClass:[NSDictionary class]]){
        [self showToast:@"参数错误" andDismiss:NO];
        return;
    }
}

- (void)showPreView{
    if(self.shareData && [self.shareData isKindOfClass:[NSDictionary class]]){
        if(self.shareData[@"image"] && [self.shareData[@"image"] isKindOfClass:[UIImage class]]){
            self.previewImg.image = self.shareData[@"image"];
            self.previewImg.hidden = NO;
            self.arView.userInteractionEnabled = NO;
        }
    }
}

- (void)hidePreview {
    self.previewImg.hidden = YES;
    self.previewImg.image = nil;
    self.arView.userInteractionEnabled = YES;
}

#pragma --mark

- (NSString *)pid{
    if(_pid)
        return _pid;
    return @"";
}

- (void)setPid:(NSString *)pid{
    _pid = [pid copy];
}

- (ARDetectType)arDetectType {
    return ARDetectTypeClound;
}


- (BOOL)dynamicDowload {
    return NO;
}

- (void)loadSourceFail:(NSError *)error {

}
- (void)loading:(NSProgress*)progress {
    _label.hidden = NO;
    _label.text = [NSString stringWithFormat:@"找到目标 正在下载%lld %%",progress.completedUnitCount];
    if(progress.totalUnitCount == progress.completedUnitCount){
        [_label removeFromSuperview];
    }
}

- (UIImageView *)previewImg{
    if(!_previewImg){
        CGFloat bottomSpace = 30;
        CGFloat leftSpace = 20;
        _previewImg = [[UIImageView alloc] initWithFrame:self.targetViewController.view.bounds];
        _previewImg.hidden = YES;
        _previewImg.userInteractionEnabled = YES;
        [self.targetViewController.view addSubview:_previewImg];
        _redrawBtn = [[UIButton alloc] initWithFrame:CGRectMake(leftSpace, _previewImg.frame.size.height - bottomSpace-btnWidth, btnWidth, btnWidth)];
        [_redrawBtn setImage:[UIImage imageNamed:@"ISDK_back"] forState:UIControlStateNormal];
        [_redrawBtn addTarget:self action:@selector(hidePreview) forControlEvents:UIControlEventTouchUpInside];
        [_previewImg addSubview:_redrawBtn];
    }

    return _previewImg;
}



- (UILabel *)toastLab{
    if(!_toastLab){
        _toastLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.targetViewController.view.frame.size.width/2, 30)];
        _toastLab.center = self.targetViewController.view.center;
        _toastLab.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        _toastLab.font = [UIFont systemFontOfSize:15];
        _toastLab.textColor = [UIColor whiteColor];
        _toastLab.textAlignment = NSTextAlignmentCenter;
        _toastLab.layer.cornerRadius = 5.0;
        _toastLab.clipsToBounds = YES;
        _toastLab.hidden = YES;
        _toastLab.numberOfLines = 0;
        [self.targetViewController.view addSubview:_toastLab];
        [self.targetViewController.view bringSubviewToFront:_toastLab];
    }
    return _toastLab;
}


- (void)showToast:(NSString*)msg andDismiss:(BOOL)dismiss{
    if(!msg.length || self.isDismissing) return;
    [self.targetViewController.view bringSubviewToFront:self.toastLab];
    self.isDismissing = dismiss;
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:15]};
    CGRect rect = [msg boundingRectWithSize:CGSizeMake(self.targetViewController.view.frame.size.width*0.7, 100) options:NSStringDrawingUsesLineFragmentOrigin |
                   NSStringDrawingUsesFontLeading attributes:dic context:nil];
    self.toastLab.frame = CGRectMake(0, 0, rect.size.width +20, rect.size.height+15);
    self.toastLab.center = self.targetViewController.view.center;
    self.toastLab.text = msg;
    self.toastLab.hidden = NO;
    self.toastLab.alpha = 1.0;
    [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.toastLab.alpha = 0.0;
    } completion:^(BOOL finished) {
        if(dismiss){
            [self back];
        }
    }];
}

#pragma --mark


- (void)back{
    [[InsightARManager shareManager] stopAllPrepare];
    [[InsightARManager shareManager] stopInsightAR];
    [self.targetViewController.navigationController popViewControllerAnimated:YES];
}

- (void)shouldPop2Home{
    if(!_shouldPop2Home) return;
    if(self.targetViewController.navigationController.viewControllers.count >2){
        NSMutableArray *afterControlers = self.targetViewController.navigationController.viewControllers.mutableCopy;
        [afterControlers removeObjectsInRange:NSMakeRange(1, self.targetViewController.navigationController.viewControllers.count - 2)];//剔除最后一个和第一个以外的vc
        self.targetViewController.navigationController.viewControllers = afterControlers;
    }
}

@end

