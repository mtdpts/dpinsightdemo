//
//  R_QRCode.m
//  DPInsightDemo
//
//  Created by tangshi on 2018/11/19.
//  Copyright © 2018 MTDP. All rights reserved.
//

#import "R_QRCode.h"

NSString * R_arscan_back = @"arscan_back";
NSString * R_arscan_button_blue = @"arscan_button_blue";
NSString * R_arscan_button_orange = @"arscan_button_orange";
NSString * R_arscan_close_gray = @"arscan_close_gray";
NSString * R_arscan_close_white = @"arscan_close_white";
NSString * R_arscan_innerring = @"arscan_innerring";
NSString * R_arscan_logo = @"arscan_logo";
NSString * R_arscan_mask = @"arscan_mask";
NSString * R_arscan_outerring = @"arscan_outerring";
NSString * R_flash_light_off = @"flash_light_off";
NSString * R_flash_light_on = @"flash_light_on";

