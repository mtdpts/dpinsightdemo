//
//  NVBaseARDelegate.h
//  DPInsightDemo
//
//  Created by tangshi on 2018/11/19.
//  Copyright © 2018 MTDP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NVARProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface CloudARDelegate : NSObject<NVARProtocol>
- (void)back;
@property (nonatomic, weak) UIViewController *targetViewController;
@property (nonatomic, strong) UIView *arView;
@property (nonatomic, copy) NSString *pid;
@end

NS_ASSUME_NONNULL_END
