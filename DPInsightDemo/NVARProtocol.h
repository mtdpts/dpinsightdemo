//
//  NVARProtocol.h
//  DPInsightDemo
//
//  Created by tangshi on 2018/11/19.
//  Copyright © 2018 MTDP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <InsightSDK/InsightSDK.h>

typedef NS_ENUM(NSUInteger, ARDetectType) {
    ARDetectTypeBase,
    ARDetectTypeClound,
    ARDetectTypeLocal
};


@protocol NVARProtocol <NSObject>

@optional


@required

@property (nonatomic, strong)UIView *arView;
@property (nonatomic, weak)UIViewController *targetViewController;
@property (nonatomic, copy)NSString *pid;

// 识别模式 base clound local
- (ARDetectType)arDetectType;
// 是否需要动态下载模型
- (BOOL)dynamicDowload;
// 加载过程
- (void)loading:(NSProgress*)progress;
//加载AR资源失败
- (void)loadSourceFail:(NSError *)error;

/**
 显示toast

 @param msg msg
 @param dismiss 弹窗消失后是否需要关闭VC
 */
- (void)showToast:(NSString*)msg andDismiss:(BOOL)dismiss;

- (void)targetViewDidLoad;

-(void)targetViewWillAppear:(BOOL)animated;

-(void)targetViewDidAppear:(BOOL)animated;


/**
 ar加载状态回调

 @param state InsightARState
 */
- (void)insightARDidUpdateARState:(InsightARState)state;


/**
 ar传给native的事件

 @param event InsightAREvent
 */
- (void)insightAREvent:(InsightAREvent *)event;


@end

@protocol ARStatusProtocol <NSObject>

@end

